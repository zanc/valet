Valet
=====

By default when you install valet via `valet install`, valet will rename your existing configuration files, e.g. `/etc/nginx/nginx.conf` to `/etc/nginx/nginx.conf.bak` which in turn will make your existing configurations not being used at all.

That's why in this tutorial I will guide you on how to install valet manually.

__This tutorial assumes that your distro is Debian (or its derivatives)__.

How to apply valet configurations manually
------------------------------------------

First, you need to install valet via composer.

`composer global require cpriego/valet-linux`

Now, create `~/.valet` and its subdirectories to store valet user configuration.

```
$ mkdir -p ~/.valet/Drivers ~/.valet/Sites ~/.valet/Extensions ~/.valet/Log ~/.valet/Certificates ~/.valet/Nginx
```

Initialize the `~/.valet` directory.

```
$ cp -p ~/.config/composer/vendor/cpriego/valet-linux/cli/stubs/SampleValetDriver.php ~/.valet/Drivers/SampleValetDriver.php
$ touch ~/.valet/Log/nginx-error.log
$ cat <<'EOF' >~/.valet/config.json
{
    "domain": "test",
    "paths": [],
    "port": "80"
}
EOF
$ touch ~/.valet/Nginx/.keep
```

Configure nginx
---------------

Configure the `/etc/nginx/nginx.conf` file.

```
user VALET_USER VALET_GROUP;

http {
    include VALET_HOME_PATH/Nginx/*;
}
```

Replace `VALET_USER` with your username (`id -un`), and `VALET_GROUP` with your group (`id -gn`). Replace `VALET_HOME_PATH` with `/home/<user>/.valet`.

Configure nginx sites-available and sites-enabled.

```
$ sed -e "s,VALET_HOME_PATH,$HOME/.valet,g" -e "s,VALET_SERVER_PATH,$HOME/.config/composer/vendor/cpriego/valet-linux/server.php,g" -e "s,VALET_STATIC_PREFIX,41c270e4-5535-4daa-b23e-c269744c2f45,g" -e "s,VALET_PORT,80,g" ~/.config/composer/vendor/cpriego/valet-linux/cli/stubs/valet.conf | sudo tee /etc/nginx/sites-available/valet.conf
# rm -f /etc/nginx/sites-enabled/default
# ln -snf /etc/nginx/sites-available/valet.conf /etc/nginx/sites-enabled/valet.conf
```

Add the following lines to `/etc/nginx/fastcgi_params`.

```
fastcgi_param  SCRIPT_FILENAME    $request_filename;
fastcgi_param  HTTP_PROXY         "";
```

Configure php-fpm
-----------------

```
$ sed -e "s,VALET_USER,$(id -un),g;s,VALET_GROUP,$(id -gn),g;s,VALET_HOME_PATH,$HOME/.valet,g" ~/.config/composer/vendor/cpriego/valet-linux/cli/stubs/fpm.conf | sudo tee /etc/php/7.0/fpm/pool.d/valet.conf
```

Restart the php-fpm service.

```
# systemctl restart php7.0-fpm
```

Configure dnsmasq
-----------------

```
# sed -i -e '/^#IGNORE_RESOLVCONF/s/#//' /etc/default/dnsmasq
# sed -i -e '\,^#conf-dir=/etc/dnsmasq.d$,s,#,,' /etc/dnsmasq.conf
# mkdir -p /etc/NetworkManager/conf.d && cat ~/.config/composer/vendor/cpriego/valet-linux/cli/stubs/networkmanager.conf >/etc/NetworkManager/conf.d/valet.conf
# systemctl stop systemd-resolved
# cat <<'EOF' >/etc/dnsmasq.d/valet
listen-address=127.0.0.1
bind-interfaces
cache-size=0
proxy-dnssec
address=/.test/127.0.0.1
EOF
```

Restart the dnsmasq service.

```
# systemctl restart dnsmasq
```

Finally
-------

Restart the nginx service.

```
# systemctl restart nginx
```

Optional
--------

The following step is not really necessary other than for safety purpose.

Put `valet` from this directory to somewhere in your `$PATH`. Now, whenever you run `valet` with any command that modifies `/etc` (e.g. `valet install`) it will prevent you from going any further.
